#include<stdio.h>

int main()
{
	int num, revNum = 0, rem;

	printf("Enter a number : ");
	scanf("%d", &num);

	while(num != 0)
	{
		rem = num % 10;
		revNum = revNum * 10 + rem;
		num /= 10;
	}

	printf("Reversed %d is %d \n", num, revNum);

	return 0;
}
