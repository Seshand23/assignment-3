#include<stdio.h>

int main()
{
	int num;

	printf("Enter a number : ");
	scanf("%d", &num);

	for(int i=2; i < num; i++)
	{
		if(num%i == 0)
		{
			printf("%d is not a prime number \n", num);

			return 0;
		}
	}

	if(num == 1)
	{
		printf("1 is not a prime number or a composite number \n");
	}
	else
	{ 
		printf("%d is a prime number \n", num);
	}

	return 0;
}
