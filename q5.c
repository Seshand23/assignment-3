#include<stdio.h>

int main()
{
	int num;

	printf("Enter number of tables : ");
	scanf("%d", &num);
	
	printf("\n\n");

	for(int i = 1 ; i <= num ; i++)
	{
		for(int j = 1 ; j <= 15 ; j++)
		{
			printf("%d X %d = %d \n", i, j, i*j);
		}
		printf("\n\n");
	}

	return 0;
}
